package de.jottyfan.minecraft.quickie.gui.action;

import java.nio.file.Path;
import java.nio.file.Paths;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class IOPath {
	/**
	 * generate absolute file name path from world, player and suffix
	 * 
	 * @param world  the world
	 * @param player the player
	 * @param suffix the suffix
	 * @return the absolute filename
	 */
	public static final String getAbsoluteFileName(World world, PlayerEntity player, String suffix) {
		StringBuilder buf = new StringBuilder();
		Path currentRelativePath = Paths.get("");
		buf.append(currentRelativePath.toAbsolutePath().toString());
		buf.append("/saves/");
		MinecraftServer server = world.getServer();
		if (server != null) {
			buf.append(server.getFolderName());
		} else {
			buf.append("unknownServerName"); // will lead to an exception as this folder is supposed not to exist
		}
		buf.append("/quickie_plan_");
		buf.append(player.getDisplayName().getFormattedText().replace(" ", "").replace("/", "")).append("_");
		buf.append(suffix).append(".nbt");
		return buf.toString();
	}
}

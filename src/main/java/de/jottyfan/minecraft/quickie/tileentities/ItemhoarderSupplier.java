package de.jottyfan.minecraft.quickie.tileentities;

import java.util.function.Supplier;

/**
 * 
 * @author jotty
 *
 */
public class ItemhoarderSupplier implements Supplier<ItemhoarderTileentity> {

	@Override
	public ItemhoarderTileentity get() {
		// TODO: is this the right way? Obviously not...
		return new ItemhoarderTileentity();
	}
}

package de.jottyfan.minecraft.quickie.items;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.util.QuickieItems;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class ItemConstruction0 extends Item {

	private Long position;
	
	public ItemConstruction0() {
		super((new Item.Properties()).group(ItemGroup.REDSTONE).maxStackSize(1));
		super.setRegistryName(QuickieMod.MODID, "construction0");
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {

		// building plans are not stackable, so we need not dropping them here
		ItemStack newStack = new ItemStack(QuickieItems.ITEM_CONSTRUCTION1);
		CompoundNBT nbt = new CompoundNBT();
		nbt.putLong("startingPos", position);
		newStack.setTag(nbt);
		playerIn.setHeldItem(handIn, newStack);
		
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
	
	@Override
	public ActionResultType onItemUse(ItemUseContext ctx) {
		BlockPos pos = ctx.getPos();
		position = pos == null ? null : pos.toLong();
		String x = new Integer(pos.getX()).toString();
		String y = new Integer(pos.getY()).toString();
		String z = new Integer(pos.getZ()).toString();
		ITextComponent msg = new TranslationTextComponent("msg.buildingplan.start", x, y, z);
		if (ctx.getWorld().isRemote) {
			ctx.getPlayer().sendMessage(msg);
		}
		return super.onItemUse(ctx);
	}
}

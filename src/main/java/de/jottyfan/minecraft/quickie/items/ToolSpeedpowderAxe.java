package de.jottyfan.minecraft.quickie.items;

import de.jottyfan.minecraft.quickie.QuickieMod;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemTier;

/**
 * 
 * @author jotty
 *
 */
public class ToolSpeedpowderAxe extends ToolRangeableAxe {
	public ToolSpeedpowderAxe() {
		super(ItemTier.DIAMOND, 4, 2.0f, (new Item.Properties()).group(ItemGroup.TOOLS));
		super.setRegistryName(QuickieMod.MODID, "speedpowderaxe");
	}
}

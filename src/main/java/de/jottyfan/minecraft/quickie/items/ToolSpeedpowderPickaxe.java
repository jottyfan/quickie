package de.jottyfan.minecraft.quickie.items;

import java.util.List;

import com.google.common.collect.Lists;

import de.jottyfan.minecraft.quickie.QuickieMod;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTier;
import net.minecraft.item.PickaxeItem;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class ToolSpeedpowderPickaxe extends PickaxeItem implements ToolRangeable {

	private HarvestRange range;

	public ToolSpeedpowderPickaxe() {
		super(ItemTier.DIAMOND, 4, 2.0f, (new Item.Properties()).group(ItemGroup.TOOLS));
		this.range = new HarvestRange(2);
		super.setRegistryName(QuickieMod.MODID, "speedpowderpickaxe");
	}

	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state) {
		return BIOMESOPLENTY_PICKAXE.contains(state.getBlock().getRegistryName()) ? this.efficiency
				: super.getDestroySpeed(stack, state);
	}

	@Override
	public HarvestRange getRange() {
		return range;
	}

	@Override
	public boolean canBreakNeigbbors(BlockState blockIn) {
		return super.canHarvestBlock(blockIn) || BIOMESOPLENTY_PICKAXE.contains(blockIn.getBlock().getRegistryName())
				|| PICKAXE_BLOCKLISTS.contains(blockIn.getBlock());
	}

	@Override
	public List<Block> getBlockList(Block block) {
		return Lists.newArrayList(block);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		CommonToolCode.onItemRightClick(worldIn, playerIn, handIn);
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		CommonToolCode.addInformation(stack, worldIn, tooltip, flagIn);
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}
}

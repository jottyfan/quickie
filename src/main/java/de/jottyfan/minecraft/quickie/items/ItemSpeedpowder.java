package de.jottyfan.minecraft.quickie.items;

import de.jottyfan.minecraft.quickie.QuickieMod;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

/**
 * 
 * @author jotty
 *
 */
public class ItemSpeedpowder extends Item {
	public ItemSpeedpowder() {
		super(new Item.Properties().group(ItemGroup.REDSTONE));
		super.setRegistryName(QuickieMod.MODID, "speedpowder");
	}
}

package de.jottyfan.minecraft.quickie.blocks;

import java.util.Arrays;
import java.util.List;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.util.QuickieItems;
import net.minecraft.block.BlockState;
import net.minecraft.block.OreBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.storage.loot.LootContext.Builder;

/**
 * 
 * @author jotty
 *
 */
public class BlockOreSulphor extends OreBlock {

	public BlockOreSulphor(Properties builder) {
		super(builder.hardnessAndResistance(1.9f));
		super.setRegistryName(QuickieMod.MODID, "oresulphor");
	}

	@Override
	public List<ItemStack> getDrops(BlockState blockState, Builder builder) {
		return Arrays.asList(new ItemStack[] { new ItemStack(QuickieItems.ITEM_SULPHOR) });
	}

	@Override
	public int getExpDrop(BlockState state, IWorldReader reader, BlockPos pos, int fortune, int silktouch) {
		return MathHelper.nextInt(RANDOM, 0, 2);
	}
}

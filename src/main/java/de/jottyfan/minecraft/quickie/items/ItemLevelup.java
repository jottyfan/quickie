package de.jottyfan.minecraft.quickie.items;

import de.jottyfan.minecraft.quickie.QuickieMod;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

/**
 * 
 * @author jotty
 *
 */
public class ItemLevelup extends Item {
	public ItemLevelup() {
		super((new Item.Properties()).group(ItemGroup.REDSTONE).maxStackSize(99));
		super.setRegistryName(QuickieMod.MODID, "levelup");
	}
}

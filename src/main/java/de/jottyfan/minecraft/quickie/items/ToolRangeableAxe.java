package de.jottyfan.minecraft.quickie.items;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.AxeItem;
import net.minecraft.item.IItemTier;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

/**
 * 
 * @author jotty
 *
 */
public class ToolRangeableAxe extends AxeItem implements ToolRangeable {

	protected ToolRangeableAxe(IItemTier tier, float p_i48530_2_, float attackSpeedIn, Properties builder) {
		super(tier, p_i48530_2_, attackSpeedIn, builder);
	}

	/**
	 * generate a block list of the resource locations in set
	 * 
	 * @param set
	 *          the set to be used as source for the block list
	 * @return a block list, at least an empty one
	 */
	private List<Block> generateBlockList(Set<ResourceLocation> set) {
		List<Block> blocks = new ArrayList<>();
		Set<ResourceLocation> rls = new HashSet<>(set); // copy to omit changes on the set
		Set<Entry<ResourceLocation, Block>> entries = ForgeRegistries.BLOCKS.getEntries();
		for (Entry<ResourceLocation, Block> entry : entries) {
			Iterator<ResourceLocation> i = rls.iterator();
			while (i.hasNext()) {
				ResourceLocation rl = i.next();
				if (rl.equals(entry.getKey())) {
					blocks.add(entry.getValue());
					i.remove(); // speed up algorithm
				}
			}
		}
		return blocks;
	}

	@Override
	public HarvestRange getRange() {
		return null; // no limit
	}

	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state) {
		return getBlockList(state.getBlock()) != null ? this.efficiency : super.getDestroySpeed(stack, state);
	}

	@Override
	public boolean canBreakNeigbbors(BlockState blockIn) {
		return getBlockList(blockIn.getBlock()) != null;
	}

	@Override
	public List<Block> getBlockList(Block block) {
		for (List<Block> blockList : AXE_BLOCKLISTS) {
			if (blockList.contains(block)) {
				return blockList;
			}
		}
		for (Set<ResourceLocation> resourceLocations : BIOMESOPLENTY_AXE) {
			if (resourceLocations.contains(block.getBlock().getRegistryName())) {
				return generateBlockList(resourceLocations);
			}
		}
		return null;
	}
}

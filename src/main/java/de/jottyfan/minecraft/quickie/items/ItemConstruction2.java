package de.jottyfan.minecraft.quickie.items;

import java.util.List;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.items.construction.ConstructionBean;
import de.jottyfan.minecraft.quickie.items.construction.ConstructionBlock;
import de.jottyfan.minecraft.quickie.util.QuickieBlocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class ItemConstruction2 extends Item {
	private BlockPos pos;

	public ItemConstruction2() {
		super((new Item.Properties()).group(ItemGroup.REDSTONE).maxStackSize(1));
		super.setRegistryName(QuickieMod.MODID, "construction2");
	}

	public ConstructionBean getConstructionBeanFromNbt(PlayerEntity player, ItemStack stack) {
		CompoundNBT nbt = stack.getTag();
		byte[] byteArray = nbt != null ? nbt.getByteArray("constructionBean") : null;
		return byteArray == null ? null : ConstructionBean.deserialize(player, byteArray);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		ConstructionBean bean = getConstructionBeanFromNbt(null, stack);
		if (bean != null && tooltip != null) {
			tooltip.add(new StringTextComponent(bean.toSummary()));
		}
	}

	private void setConstructionBeanToNbt(ItemStack stack, ConstructionBean bean) {
		byte[] constructionBeanByteArray = bean.serialize();
		CompoundNBT nbt = stack.getTag();
		nbt.putByteArray("constructionBean", constructionBeanByteArray);
	}

	/**
	 * check if the item is part of the inventory
	 * 
	 * @param item      the item
	 * @param itemStack the itemStack
	 * @return true or false
	 */
	public boolean hasItemInItsInventory(Item item, ItemStack itemStack) {
		ConstructionBean bean = getConstructionBeanFromNbt(null, itemStack);
		for (ConstructionBlock block : bean.getList()) {
			Item currentItem = block.getBlockState().getBlock().asItem();
			if (currentItem.equals(item)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * replace itemstack in building plan
	 * 
	 * @param planStack  the stack to be manipulated
	 * @param trashStack the stack of items that are for the trash
	 * @param newStack   the stack of items as replacement
	 * @return null if failed or the manipulated planStack if successful
	 * @throws ConstructionException on errors
	 */
	public ItemStack replaceStacks(ItemStack planStack, ItemStack trashStack, ItemStack newStack)
			throws ConstructionException {
		try {
			ItemConstruction2 item = (ItemConstruction2) planStack.getItem();
			ConstructionBean bean = item.getConstructionBeanFromNbt(null, planStack);
			String result = bean.replaceAllStacksOf(trashStack, newStack);
			if (result == null) {
				item.setConstructionBeanToNbt(planStack, bean);
			} else {
				throw new ConstructionException(result);
			}
			return planStack;
		} catch (ClassCastException e) {
			throw new ConstructionException("planStack is not of ItemConstruction2");
		}
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		ItemStack stack = playerIn.getHeldItem(handIn);
		ConstructionBean bean = getConstructionBeanFromNbt(playerIn, stack);
		if (bean == null) {
			ITextComponent msg = new TranslationTextComponent("msg.buildingplan.null");
			if (worldIn.isRemote) {
				playerIn.sendMessage(msg);
			}
		} else if (pos == null) { // only fill up plan
		  bean.fillBlocksFromInventory(playerIn);
		  byte[] constructionBeanByteArray = bean.serialize();
      CompoundNBT nbt = stack.getTag();
      nbt.putByteArray("constructionBean", constructionBeanByteArray);
	  } else if (playerIn.isSneaking()) {
			bean.dropBlocksOntoGround(worldIn, pos);
		} else {
			ResourceLocation found = worldIn.getBlockState(pos).getBlock().getRegistryName();
			if (found.equals(QuickieBlocks.ROTATECLOCKWISE.getRegistryName())) {
				bean.rotateClockwise(playerIn, worldIn);
			} else if (found.equals(QuickieBlocks.ROTATECOUNTERCLOCKWISE.getRegistryName())) {
				bean.rotateCounterclockwise(playerIn, worldIn);
			} else if (found.equals(QuickieBlocks.MIRRORHORIZONTAL.getRegistryName())) {
				bean.mirrorHorizontal(playerIn, worldIn);
			} else if (found.equals(QuickieBlocks.MIRRORVERTICAL.getRegistryName())) {
				bean.mirrorVertical(playerIn, worldIn);
			} else if (found.equals(QuickieBlocks.MENU.getRegistryName())) {
				if (worldIn.isRemote) {
					playerIn.sendMessage(new StringTextComponent("not yet implemented"));
				}
			} else if (found.equals(QuickieBlocks.MOVEUP.getRegistryName())) {
				bean.move(playerIn, worldIn, 1);
			} else if (found.equals(QuickieBlocks.MOVEDOWN.getRegistryName())) {
				bean.move(playerIn, worldIn, -1);
			} else {
				bean.fillBlocksFromInventory(playerIn);
				bean.buildInWorldOrFail(worldIn, playerIn, pos);
			}
			byte[] constructionBeanByteArray = bean.serialize();
			CompoundNBT nbt = stack.getTag();
			nbt.putByteArray("constructionBean", constructionBeanByteArray);
		}
		pos = null;
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		ConstructionBean bean = getConstructionBeanFromNbt(null, stack);
		return bean == null ? false : !bean.isFilled();
	}

	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		ConstructionBean bean = getConstructionBeanFromNbt(null, stack);
		int total = bean.getNumberOfTotalBlocks();
		int missing = bean.getNumberOfMissingBlocks();
		return total <= 0 ? 0.99 : (missing >= total ? 0.99 : ((double) missing / (double) total));
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext ctx) {
		pos = ctx.getPos();
		return super.onItemUse(ctx);
	}
}

package de.jottyfan.minecraft.quickie.items.construction;

import java.io.Serializable;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;

/**
 * 
 * @author jotty
 *
 */
public class ConstructionBlock implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BlockPos pos;
	private BlockState blockState;
	private boolean isSet;

	public ConstructionBlock(BlockPos pos, BlockState blockState) {
		super();
		this.pos = pos;
		this.blockState = blockState;
		this.isSet = false;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder("ConstructionBlock@");
		buf.append(pos).append("{");
		buf.append(blockState);
		buf.append("}");
		return buf.toString();
	}
	
	public byte[] toByteArray() {
		return new ConstructionBlockSerializer().getBytes(pos, blockState, isSet);
	}

	public static final ConstructionBlock fromByteArray(PlayerEntity player, byte[] array) {
		return new ConstructionBlockSerializer().getConstructionBlock(player, array);
	}

	public void resetBlockPos(int x, int y, int z) {
		pos = new BlockPos(x, y, z);
	}
	
	public void mirror(Mirror mirror) {
		setBlockState(blockState.mirror(mirror));
	}
	
	public void rotate(IWorld world, Rotation rotation) {
		setBlockState(blockState.rotate(world, pos, rotation));
	}

	public BlockPos getPos() {
		return pos;
	}

	public void setBlockState(BlockState blockState) {
		this.blockState = blockState;
	}
	
	public BlockState getBlockState() {
		return blockState;
	}

	public boolean isSet() {
		return isSet;
	}

	public void setSet(boolean isSet) {
		this.isSet = isSet;
	}
}

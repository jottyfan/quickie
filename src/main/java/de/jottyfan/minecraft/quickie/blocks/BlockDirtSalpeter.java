package de.jottyfan.minecraft.quickie.blocks;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.util.QuickieItems;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.GravelBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.storage.loot.LootContext.Builder;

/**
 * 
 * @author jotty
 *
 */
public class BlockDirtSalpeter extends GravelBlock {

	public BlockDirtSalpeter(Properties builder) {
		super(builder.hardnessAndResistance(3.1f));
		super.setRegistryName(QuickieMod.MODID, "dirtsalpeter");
	}

	@Override
	public List<ItemStack> getDrops(BlockState blockState, Builder builder) {
		Integer count = (Double.valueOf(new Random().nextDouble() * 10).intValue() / 4) + 1;
		boolean spawnBoneMeal = new Random().nextDouble() > 0.9f;
		ItemStack boneMealStack = new ItemStack(Items.BONE_MEAL);
		ItemStack salpeterStack = new ItemStack(QuickieItems.ITEM_SALPETER, count);
		ItemStack dirtStack = new ItemStack(Blocks.DIRT);
		ItemStack[] spawnies = spawnBoneMeal ? new ItemStack[] { boneMealStack, salpeterStack, dirtStack }
				: new ItemStack[] { salpeterStack, dirtStack };
		return Arrays.asList(spawnies);
	}

	@Override
	public int getExpDrop(BlockState state, IWorldReader reader, BlockPos pos, int fortune, int silktouch) {
		return MathHelper.nextInt(RANDOM, 0, 2);
	}
}

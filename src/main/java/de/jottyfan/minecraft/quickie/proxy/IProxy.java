package de.jottyfan.minecraft.quickie.proxy;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.eventbus.api.IEventBus;

/**
 * 
 * @author jotty
 *
 */
public interface IProxy {
	
	/**
	 * register custom model of item with special metadata
	 * 
	 * @param item to be registered
	 * @param metadata to be used for registration
	 * @param registryName to be used for registration
	 */
	public void registerCustomModel(Item item, int metadata, ResourceLocation registryName);

	/**
	 * register event on one side of the proxy
	 * 
	 * @param bus
	 * @param eventFaceBlock
	 */
	public void registerClientEventFaceBlock(IEventBus bus);
}

package de.jottyfan.minecraft.quickie.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.ResourceLocation;

/**
 * 
 * @author jotty
 *
 */
public interface ToolRangeable {
	public static final Set<Block> SHOVEL_EFFECTIVE_ON = Sets.newHashSet(new Block[] { Blocks.GRAVEL, Blocks.SAND,
			Blocks.GRASS_BLOCK, Blocks.DIRT, Blocks.CLAY, Blocks.FARMLAND, Blocks.GRASS_PATH, Blocks.RED_SAND, Blocks.SOUL_SAND });

	public static final List<List<Block>> AXE_BLOCKLISTS = Lists.newArrayList(
			Arrays.asList(Blocks.ACACIA_LOG, Blocks.STRIPPED_ACACIA_LOG, Blocks.ACACIA_WOOD, Blocks.STRIPPED_ACACIA_WOOD),
			Arrays.asList(Blocks.BIRCH_LOG, Blocks.STRIPPED_BIRCH_LOG, Blocks.BIRCH_WOOD, Blocks.STRIPPED_BIRCH_WOOD),
			Arrays.asList(Blocks.DARK_OAK_LOG, Blocks.STRIPPED_DARK_OAK_LOG, Blocks.DARK_OAK_WOOD,
					Blocks.STRIPPED_DARK_OAK_WOOD),
			Arrays.asList(Blocks.JUNGLE_LOG, Blocks.STRIPPED_JUNGLE_LOG, Blocks.JUNGLE_WOOD, Blocks.STRIPPED_JUNGLE_WOOD),
			Arrays.asList(Blocks.OAK_LOG, Blocks.STRIPPED_OAK_LOG, Blocks.OAK_WOOD, Blocks.STRIPPED_OAK_WOOD),
			Arrays.asList(Blocks.SPRUCE_LOG, Blocks.STRIPPED_SPRUCE_LOG, Blocks.SPRUCE_WOOD, Blocks.STRIPPED_SPRUCE_WOOD),
			Arrays.asList(Blocks.ACACIA_PLANKS), Arrays.asList(Blocks.BIRCH_PLANKS), Arrays.asList(Blocks.DARK_OAK_PLANKS),
			Arrays.asList(Blocks.JUNGLE_PLANKS), Arrays.asList(Blocks.OAK_PLANKS), Arrays.asList(Blocks.SPRUCE_PLANKS),
			Arrays.asList(Blocks.ACACIA_SLAB), Arrays.asList(Blocks.BIRCH_SLAB), Arrays.asList(Blocks.DARK_OAK_SLAB),
			Arrays.asList(Blocks.JUNGLE_SLAB), Arrays.asList(Blocks.OAK_SLAB), Arrays.asList(Blocks.SPRUCE_SLAB),
			Arrays.asList(Blocks.ACACIA_STAIRS), Arrays.asList(Blocks.BIRCH_STAIRS), Arrays.asList(Blocks.DARK_OAK_STAIRS),
			Arrays.asList(Blocks.JUNGLE_STAIRS), Arrays.asList(Blocks.OAK_STAIRS), Arrays.asList(Blocks.SPRUCE_STAIRS),
			Arrays.asList(Blocks.POTTED_BROWN_MUSHROOM, Blocks.BROWN_MUSHROOM_BLOCK, Blocks.BROWN_MUSHROOM,
					Blocks.MUSHROOM_STEM),
			Arrays.asList(Blocks.POTTED_RED_MUSHROOM, Blocks.RED_MUSHROOM_BLOCK, Blocks.RED_MUSHROOM, Blocks.MUSHROOM_STEM));
	
	public static final Set<Block> PICKAXE_BLOCKLISTS = Sets.newHashSet(new Block[] {Blocks.GLOWSTONE});

	public static final Set<ResourceLocation> BIOMESOPLENTY_SHOVEL = Sets
			.newHashSet(new ResourceLocation[] { new ResourceLocation("biomesoplenty", "flesh"),
					new ResourceLocation("biomesoplenty", "dirt"), new ResourceLocation("biomesoplenty", "grass"),
					new ResourceLocation("biomesoplenty", "mud"), new ResourceLocation("biomesoplenty", "white_sand") });

	public static final Set<ResourceLocation> BIOMESOPLENTY_PICKAXE = Sets
			.newHashSet(new ResourceLocation[] { new ResourceLocation("biomesoplenty", "dried_sand") });

	public static final List<Set<ResourceLocation>> BIOMESOPLENTY_AXE = Lists.newArrayList(generateBOPAxeSet());

	static List<Set<ResourceLocation>> generateBOPAxeSet() {
		List<Set<ResourceLocation>> list = new ArrayList<>();
		String[] bOPLogs = new String[] { "cherry", "dead", "ethereal", "fir", "hellbark", "jacaranda", "magic", "mahogany",
				"palm", "redwood", "umbran", "willow" };
		for (String s : bOPLogs) {
			Set<ResourceLocation> set = new HashSet<>();
			set.add(new ResourceLocation("biomesoplenty", s + "_log"));
			set.add(new ResourceLocation("biomesoplenty", s + "_wood"));
			set.add(new ResourceLocation("biomesoplenty", "stripped_" + s + "_log"));
			set.add(new ResourceLocation("biomesoplenty", "stripped_" + s + "_wood"));
			list.add(set);
			list.add(Sets.newHashSet(new ResourceLocation("biomesoplenty", s + "_stairs")));
			list.add(Sets.newHashSet(new ResourceLocation("biomesoplenty", s + "_planks")));
			list.add(Sets.newHashSet(new ResourceLocation("biomesoplenty", s + "_slab")));
		}
		return list;
	}

	/**
	 * @return range of blocks to be harvested
	 */
	public HarvestRange getRange();

	/**
	 * check if this block state is one that affects the neighbor blocks to break
	 * also if they are from the same type
	 * 
	 * @param blockState
	 *          the block state of the current block
	 * @return true or false
	 */
	public boolean canBreakNeigbbors(BlockState blockState);

	/**
	 * get list of blocks that belong together
	 * 
	 * @param block
	 *          of the set
	 * @return the list of blocks or null if not found
	 */
	public List<Block> getBlockList(Block block);
}

package de.jottyfan.minecraft.quickie.gui.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class SaveButtonAction {

	private final World world;
	private final PlayerEntity player;
	private final ItemStack stack;
	private final String suffix;

	public SaveButtonAction(World world, PlayerEntity player, ItemStack stack, String suffix) {
		this.world = world;
		this.player = player;
		this.stack = stack;
		this.suffix = suffix;
	}

	public void execute(double x, double y) {
		String fileName = IOPath.getAbsoluteFileName(world, player, suffix);
		String msg = "nothing happened";
		try {
			File file = new File(fileName);
			if (file.exists() && !file.delete()) {
				msg = "could not recreate file " + fileName;
			} else {
				CompoundNBT nbt = stack.getTag();
				CompressedStreamTools.writeCompressed(nbt, new FileOutputStream(file));
				msg = "saved buildingplan to " + fileName;
			}
		} catch (IOException e) {
			msg = e.getMessage();
		}
		if (!world.isRemote) {
			player.sendMessage(new StringTextComponent(msg));
		}
	}
}

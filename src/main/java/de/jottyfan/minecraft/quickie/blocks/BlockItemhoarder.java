package de.jottyfan.minecraft.quickie.blocks;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.tileentities.ItemhoarderTileentity;
import net.minecraft.block.BlockState;
import net.minecraft.block.FallingBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class BlockItemhoarder extends FallingBlock {

	public BlockItemhoarder(Properties properties) {
		super(properties.hardnessAndResistance(2.5f));
		super.setRegistryName(QuickieMod.MODID, "itemhoarder");
	}

	public boolean hasTileEntity(BlockState state) {
		return true;
	}

	@Override
	public void onEndFalling(World world, BlockPos pos, BlockState fallingState, BlockState hitState) {
		super.onEndFalling(world, pos, fallingState, hitState);
		if (world.isRemote) {
			TileEntity tileEntity = world.getTileEntity(pos);
			if (tileEntity instanceof ItemhoarderTileentity) {
				ItemhoarderTileentity it = (ItemhoarderTileentity) tileEntity;
				boolean found = false;
				for (ItemStack stack : it.getStacks()) {
					if (stack.getItem().getRegistryName().equals(this.getRegistryName())) {
						if (!found) {
							stack.setCount(stack.getCount() - 1); // remove the one that is falling
							found = true;
						}
					}
				}
			}
		}
	}

	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		return new ItemhoarderTileentity();
	}

	@Override
	public boolean onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
			BlockRayTraceResult brtr) {
		if (world.isRemote) {
			TileEntity tileEntity = world.getTileEntity(pos);
			if (tileEntity instanceof ItemhoarderTileentity) {
				ItemhoarderTileentity it = (ItemhoarderTileentity) tileEntity;
				ITextComponent message = new TranslationTextComponent("msg.itemhoarder.summary", it.getSummary());
				player.sendMessage(message);
				return true;
			}
		}
		return false;
	}
}

package de.jottyfan.minecraft.quickie.world;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.jottyfan.minecraft.quickie.util.QuickieBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.BlockWithContextConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig.FillerBlockType;
import net.minecraft.world.gen.placement.ChanceRangeConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;

/**
 * 
 * @author jotty
 *
 */
public class OreGenerator {
	private static final Logger LOGGER = LogManager.getLogger(OreGenerator.class);

	/**
	 * register quickie ores to the biomes in the collection
	 * 
	 * @param biomes
	 */
	public void registerOres(Collection<Biome> biomes) {
		for (Biome biome : biomes) {
			if (biome.getRegistryName().equals(Biomes.NETHER.getRegistryName())) {
				LOGGER.debug("register quickie ores for nether biome {}", biome.getRegistryName().getPath());
				addFeaturedOreGenerationForNetherrack(Decoration.UNDERGROUND_ORES, biome, QuickieBlocks.ORE_NETHER_SULPHOR, 24,
						0.5F, 0, 0, 128);
			} else {
				LOGGER.debug("register quickie ores for biome {}", biome.getRegistryName().getPath());
				BlockState[] SANDLIKE = new BlockState[] { Blocks.SAND.getDefaultState(), Blocks.SANDSTONE.getDefaultState(),
						Blocks.SANDSTONE_WALL.getDefaultState(), Blocks.CHISELED_SANDSTONE.getDefaultState() };
				BlockState[] DIRTLIKE = new BlockState[] { Blocks.DIRT.getDefaultState() };
				BlockState[] GRASSLIKE = new BlockState[] { Blocks.GRASS.getDefaultState(),
						Blocks.GRASS_BLOCK.getDefaultState(), Blocks.GRASS_PATH.getDefaultState() };

				addFeaturedOreGenerationWithContext(Decoration.UNDERGROUND_ORES, biome, SANDLIKE, SANDLIKE, SANDLIKE,
						QuickieBlocks.ORE_SAND_SALPETER, 10, 64, 196, 255);
				addFeaturedOreGenerationWithContext(Decoration.UNDERGROUND_ORES, biome, SANDLIKE, SANDLIKE, SANDLIKE,
						QuickieBlocks.SAND_SALPETER, 10, 64, 196, 255);
				addFeaturedOreGenerationWithContext(Decoration.UNDERGROUND_ORES, biome, DIRTLIKE, DIRTLIKE, GRASSLIKE,
						QuickieBlocks.DIRT_SALPETER, 16, 64, 196, 255);
				addFeaturedOreGenerationForNaturalStone(Decoration.UNDERGROUND_ORES, biome, QuickieBlocks.ORE_SULPHOR, 16, 4, 4,
						196, 255);
				addFeaturedOreGenerationForNaturalStone(Decoration.UNDERGROUND_ORES, biome, QuickieBlocks.ORE_SALPETER, 12, 10,
						4, 196, 255);
			}
		}
	}

	private void addFeaturedOreGenerationWithContext(Decoration decoration, Biome biome, BlockState[] placeOn,
			BlockState[] placeIn, BlockState[] placeUnder, Block block, int nrPerChunk, int minHeight, int maxHeightBase,
			int maxHeight) {
		BlockWithContextConfig config = new BlockWithContextConfig(block.getDefaultState(), placeOn, placeIn, placeUnder);
		ConfiguredFeature<?> featureConfig = Biome.createDecoratedFeature(Feature.SIMPLE_BLOCK, config,
				Placement.COUNT_RANGE, new CountRangeConfig(nrPerChunk, minHeight, maxHeightBase, maxHeight));
		biome.addFeature(decoration, featureConfig);
	}

	private void addFeaturedOreGenerationForNaturalStone(Decoration decoration, Biome biome, Block block, int size,
			int nrPerChunk, int minHeight, int maxHeightBase, int maxHeight) {
		OreFeatureConfig oreFeatureConfig = new OreFeatureConfig(FillerBlockType.NATURAL_STONE, block.getDefaultState(),
				size);
		ConfiguredFeature<?> featureConfig = Biome.createDecoratedFeature(Feature.ORE, oreFeatureConfig,
				Placement.COUNT_RANGE, new CountRangeConfig(nrPerChunk, minHeight, maxHeightBase, maxHeight));
		biome.addFeature(decoration, featureConfig);
	}

	private void addFeaturedOreGenerationForNetherrack(Decoration decoration, Biome biome, Block block, int size,
			float chance, int bottomOffset, int topOffset, int top) {
		OreFeatureConfig oreFeatureConfig = new OreFeatureConfig(FillerBlockType.NETHERRACK, block.getDefaultState(), size);
		ConfiguredFeature<?> featureConfig = Biome.createDecoratedFeature(Feature.ORE, oreFeatureConfig,
				Placement.CHANCE_RANGE, new ChanceRangeConfig(chance, bottomOffset, topOffset, top));
		biome.addFeature(decoration, featureConfig);
	}
}

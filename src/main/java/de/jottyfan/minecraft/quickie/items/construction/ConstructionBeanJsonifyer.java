package de.jottyfan.minecraft.quickie.items.construction;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @author jotty
 *
 */
public class ConstructionBeanJsonifyer {

	/**
	 * generate json representation of construction block
	 * 
	 * @param bean
	 *          the construction bean
	 * @return the json string
	 */
	public String toJson(ConstructionBean bean) {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(bean);
	}

	/**
	 * generate construction block from json string
	 * 
	 * @param json
	 *          the string
	 * @return the construction bean if possible (mods might be missing)
	 */
	public ConstructionBean fromJson(String json) {
		Gson gson = new GsonBuilder().create();
		return gson.fromJson(json, ConstructionBean.class);
	}
}

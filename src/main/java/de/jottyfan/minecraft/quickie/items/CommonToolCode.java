package de.jottyfan.minecraft.quickie.items;

import java.util.List;

import de.jottyfan.minecraft.quickie.util.QuickieItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class CommonToolCode {
	public static final void onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		CompoundNBT nbt = playerIn.getHeldItem(handIn).getTag();
		if (nbt != null) {
			int level = nbt.getInt("level");
			if (playerIn.isSneaking()) {
				// drop all enhancements into the players inventory if possible
				ItemStack stack = new ItemStack(QuickieItems.ITEM_LEVELUP, level);
				boolean unlevelingerror = false;
				if (!worldIn.isRemote) {
					if (playerIn.addItemStackToInventory(stack)) {
						level = 0;
					} else {
						unlevelingerror = true;
					}
				}
				if (worldIn.isRemote && unlevelingerror) {
					playerIn.sendMessage(new TranslationTextComponent("error.unleveling.inventory.full"));
				}
			} else {
				// add all enhancements in players inventory
				if (!worldIn.isRemote) {
					NonNullList<ItemStack> main = playerIn.inventory.mainInventory;
					for (ItemStack stack : main) {
						if (stack.getItem().equals(QuickieItems.ITEM_LEVELUP)) {
							level += stack.getCount();
							stack.setCount(0);
						}
					}
				}
			}
			nbt.putInt("level", level);
		}
	}

	public static final void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		if (tooltip != null) {
			CompoundNBT nbt = stack.getTag();
			if (nbt != null) {
				tooltip.add(new StringTextComponent("level " + nbt.getInt("level")));
			} else {
				tooltip.add(new StringTextComponent("level 0"));
			}
		}
	}
}

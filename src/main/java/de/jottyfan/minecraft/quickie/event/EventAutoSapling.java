package de.jottyfan.minecraft.quickie.event;

import net.minecraft.block.Block;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.item.Item;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.event.entity.item.ItemExpireEvent;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * 
 * @author jotty
 *
 */
public class EventAutoSapling {

	/**
	 * get item from entityItem if not null
	 * 
	 * @param itemEntity
	 *          to be used
	 * @return item if not null
	 */
	private Item getItemFromItemEntity(ItemEntity itemEntity) {
		return (itemEntity == null || itemEntity.getItem() == null) ? null : itemEntity.getItem().getItem();
	}

	/**
	 * get block from item if item is plantable
	 * 
	 * @param item
	 *          to be used
	 * @return block if found and plantable, null otherwise
	 */
	private Block getBlockIfPlantable(Item item) {
		Block block = Block.getBlockFromItem(item);
		if (block instanceof IPlantable) {
			return block;
		} else {
			return null;
		}
	}

	@SubscribeEvent
	public void itemDecay(ItemExpireEvent event) {
		Item item = getItemFromItemEntity(event.getEntityItem());
		if (item != null) {
			Block block = getBlockIfPlantable(item);
			IPlantable iPlantable = (IPlantable) block;
			if (block != null) {
				BlockPos pos = event.getEntity().getPosition();
				World world = event.getEntityItem().world;
				if (world.getBlockState(pos).canSustainPlant(world, pos, Direction.DOWN, iPlantable)) {
					event.getEntityItem().world.setBlockState(pos, block.getDefaultState());
				}
			}
		}
	}

	@SubscribeEvent
	public void itemToss(ItemTossEvent event) {
		Item item = getItemFromItemEntity(event.getEntityItem());
		if (item != null && getBlockIfPlantable(item) != null) {
			event.getEntityItem().lifespan = 100; // despawn time
		}
	}
}

package de.jottyfan.minecraft.quickie.blocks;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.util.QuickieItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ExperienceOrbEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.storage.loot.LootContext.Builder;

/**
 * 
 * @author jotty
 *
 */
public class BlockLavahoarder extends Block {

	public BlockLavahoarder(Properties properties) {
		super(properties.hardnessAndResistance(2.5f));
		super.setRegistryName(QuickieMod.MODID, "lavahoarder");
	}

	@Override
	public List<ItemStack> getDrops(BlockState blockState, Builder builder) {
		return Arrays.asList(new ItemStack[] { new ItemStack(Item.BLOCK_TO_ITEM.get(this)) });
	}

	private static final String stringOf(BlockPos pos) {
		StringBuilder buf = new StringBuilder();
		buf.append(pos.getX()).append(":");
		buf.append(pos.getY()).append(":");
		buf.append(pos.getZ());
		return buf.toString();
	}

	private static final BlockPos blockPosOf(String s) {
		if (s.contains(":")) {
			String[] parts = s.split(":");
			if (parts.length > 2) {
				Integer x = Integer.valueOf(parts[0]);
				Integer y = Integer.valueOf(parts[1]);
				Integer z = Integer.valueOf(parts[2]);
				return new BlockPos(x, y, z);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private void findAllAttachedLavaBlocks(Set<String> list, BlockPos pos, World world) {
		if (Blocks.LAVA.equals(world.getBlockState(pos).getBlock())) {
			String p = stringOf(pos);
			if (!list.contains(p)) {
				list.add(p);
				findAllAttachedLavaBlocks(list, pos.up(), world);
				findAllAttachedLavaBlocks(list, pos.down(), world);
				findAllAttachedLavaBlocks(list, pos.north(), world);
				findAllAttachedLavaBlocks(list, pos.south(), world);
				findAllAttachedLavaBlocks(list, pos.east(), world);
				findAllAttachedLavaBlocks(list, pos.west(), world);
			}
		}
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {
		if (placer.dimension.getId() == DimensionType.OVERWORLD.getId()) { // do this only in the overworld
			Set<String> positions = new HashSet<>();
			findAllAttachedLavaBlocks(positions, pos.up(), world);
			findAllAttachedLavaBlocks(positions, pos.down(), world);
			findAllAttachedLavaBlocks(positions, pos.north(), world);
			findAllAttachedLavaBlocks(positions, pos.south(), world);
			findAllAttachedLavaBlocks(positions, pos.east(), world);
			findAllAttachedLavaBlocks(positions, pos.west(), world);
			Integer amount = positions.size();
			for (String p : positions) {
				world.setBlockState(blockPosOf(p), Blocks.AIR.getDefaultState());
			}
			Random random = new Random();
			if (amount > 0) {
				int count = 0;
				for (int i = 0; i < amount; i++) {
					if (random.nextFloat() < 0.125) {
						count++;
					}
				}
				ItemStack sulphor = new ItemStack(QuickieItems.ITEM_SULPHOR, count);
				Block.spawnAsEntity(world, pos.up(), sulphor);
				world.addEntity(new ExperienceOrbEntity(world, (double)pos.getX() + 0.5D, (double)pos.getY() + 0.5D, (double)pos.getZ() + 0.5D, count));
			}
		} else {
			onBlockPlacedBy(world, pos, state, placer, stack);
		}
	}
}

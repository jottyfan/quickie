package de.jottyfan.minecraft.quickie.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public abstract class BlockFallingContainer extends Block {

	protected BlockFallingContainer(Properties properties) {
		super(properties);
	}

	public abstract void handleTileEntityDrops(World world, TileEntity tileEntity, BlockPos pos);

	public void onBlockAdded(World worldIn, BlockPos pos, BlockState state) {
		worldIn.getPendingBlockTicks().scheduleTick(pos, this, this.tickRate(worldIn));
	}

	public BlockState updatePostPlacement(BlockState stateIn, BlockState facingState, IWorld worldIn, BlockPos currentPos,
			BlockPos facingPos) {
		worldIn.getPendingBlockTicks().scheduleTick(currentPos, this, this.tickRate(worldIn));
		return stateIn;
	}

	public void tick(BlockState state, World worldIn, BlockPos pos, Random random) {
		if (!worldIn.isRemote) {
			this.checkFallable(worldIn, pos);
		}
	}

	private void checkFallable(World worldIn, BlockPos pos) {
		if (canFallThrough(worldIn.getBlockState(pos.down())) && pos.getY() >= 0) {
			BlockState state = getDefaultState();
			if (worldIn.getBlockState(pos).getBlock() == this) {
				state = worldIn.getBlockState(pos);
				TileEntity tileEntity = worldIn.getTileEntity(pos);
				if (tileEntity == null) {
				} else {
					handleTileEntityDrops(worldIn, tileEntity, pos);
				}
				worldIn.removeBlock(pos, false);
			}
			BlockPos blockpos;
			for (blockpos = pos.down(); canFallThrough(worldIn.getBlockState(blockpos))
					&& blockpos.getY() > 0; blockpos = blockpos.down()) {
				;
			}
			if (blockpos.getY() > 0) {
				worldIn.setBlockState(blockpos.up(), state);
			}
		}
	}

	public int tickRate(World worldIn) {
		return 2;
	}

	public static boolean canFallThrough(BlockState state) {
		Block block = state.getBlock();
		Material material = state.getMaterial();
		return block == Blocks.AIR || block == Blocks.FIRE || material.isLiquid() || material.isReplaceable();
	}

	public void onEndFalling(World worldIn, BlockPos pos, BlockState p_176502_3_, BlockState p_176502_4_) {
	}

	public void onBroken(World worldIn, BlockPos pos) {
	}
}

package de.jottyfan.minecraft.quickie.items.construction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.jottyfan.minecraft.quickie.util.QuickieBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Mirror;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.World;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;

/**
 * 
 * @author jotty
 *
 */
public class ConstructionBean {
	private final List<ConstructionBlock> list;
	private String name;

	public ConstructionBean(World world, BlockPos start, BlockPos end) {
		super();
		this.list = new ArrayList<>();
		createConstructionPlan(world, start, end);
	}

	protected ConstructionBean(List<ConstructionBlock> list) {
		super();
		this.list = list;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder("ConstructionBean{");
		for (ConstructionBlock block : list) {
			buf.append(block);
		}
		buf.append("}");
		return buf.toString();
	}

	/**
	 * get the boundaries of the construction plan
	 * 
	 * @return the extremes (minimum and maximum corners)
	 */
	public Map<EnumExtreme, BlockPos> getBoundaries() {
		int maxx = 0, maxy = 0, maxz = 0, minx = 0, miny = 0, minz = 0;
		boolean firstIteration = true;
		for (ConstructionBlock block : list) {
			maxx = (firstIteration || block.getPos().getX() > maxx) ? block.getPos().getX() : maxx;
			maxy = (firstIteration || block.getPos().getY() > maxy) ? block.getPos().getY() : maxy;
			maxz = (firstIteration || block.getPos().getZ() > maxz) ? block.getPos().getZ() : maxz;
			minx = (firstIteration || block.getPos().getX() < minx) ? block.getPos().getX() : minx;
			miny = (firstIteration || block.getPos().getY() < miny) ? block.getPos().getY() : miny;
			minz = (firstIteration || block.getPos().getZ() < minz) ? block.getPos().getZ() : minz;
			firstIteration = false;
		}
		Map<EnumExtreme, BlockPos> map = new HashMap<>();
		map.put(EnumExtreme.MIN, new BlockPos(minx, miny, minz));
		map.put(EnumExtreme.MAX, new BlockPos(maxx, maxy, maxz));
		return map;
	}

	/**
	 * build the building on the building plan at pos
	 * 
	 * @param worldIn the world
	 * @param pos     the position
	 */
	public void build(World worldIn, BlockPos pos) {
		for (ConstructionBlock cBlock : list) {
			BlockPos buildPos = pos.add(cBlock.getPos());
			boolean built = buildBlock(worldIn, buildPos, cBlock.getBlockState());
			if (built) {
				cBlock.setSet(false);
			}
		}
	}

	/**
	 * build a block at pos
	 * 
	 * @param worldIn
	 * @param pos
	 * @param blockState
	 */
	private boolean buildBlock(World worldIn, BlockPos pos, BlockState blockState) {
		BlockState oldState = worldIn.getBlockState(pos);
		worldIn.destroyBlock(pos, true);
		boolean done = worldIn.setBlockState(pos, blockState);
		if (done) {
			worldIn.notifyBlockUpdate(pos, oldState, blockState, 3); // flags as in setBlockState(BlockPos, IBlockState)
		}
		return done;
	}

	/**
	 * create the construction plan from the world
	 * 
	 * @param world the world
	 * @param start the starting position
	 * @param end   the ending position
	 */
	private void createConstructionPlan(World world, BlockPos start, BlockPos end) {
		Integer x1 = start.getX() > end.getX() ? end.getX() : start.getX();
		Integer x2 = start.getX() > end.getX() ? start.getX() : end.getX();
		Integer y1 = start.getY() > end.getY() ? end.getY() : start.getY();
		Integer y2 = start.getY() > end.getY() ? start.getY() : end.getY();
		Integer z1 = start.getZ() > end.getZ() ? end.getZ() : start.getZ();
		Integer z2 = start.getZ() > end.getZ() ? start.getZ() : end.getZ();
		for (int x = x1; x <= x2; x++) {
			for (int y = y1; y <= y2; y++) {
				for (int z = z1; z <= z2; z++) {
					BlockPos pos = new BlockPos(x, y, z);
					BlockState blockState = world.getBlockState(pos);
					Boolean ignore = blockState.isAir(world, pos);
					ignore = ignore || blockState.getBlock().equals(Blocks.WATER);
					ignore = ignore || blockState.getBlock().equals(Blocks.LAVA);
					ignore = ignore || blockState.getBlock().equals(QuickieBlocks.CONSTRUCTIONBORDER);
					if (!ignore) {
						Integer xDiff = x - start.getX();
						Integer yDiff = y - start.getY();
						Integer zDiff = z - start.getZ();
						BlockPos newPos = new BlockPos(xDiff, yDiff, zDiff);
						blockState = replaceBlockState(blockState);
						list.add(new ConstructionBlock(newPos, blockState));
					}
				}
			}
		}
	}

	/**
	 * replace some blocks
	 * 
	 * @param blockState block to be checked
	 * @return block itself or replacement
	 */
	private BlockState replaceBlockState(BlockState blockState) {
		Block block = blockState.getBlock();
		if (block.equals(Blocks.GRASS_BLOCK)) {
			return Blocks.DIRT.getDefaultState();
		} else if (block.equals(Blocks.BEDROCK)) {
			return Blocks.STONE.getDefaultState();
		}
		return blockState;
	}

	/**
	 * add block requirement to the building plan
	 * 
	 * @param blockState the needed block
	 * @param pos        the position
	 */
	public void addBlock(BlockState blockState, BlockPos pos) {
		list.add(new ConstructionBlock(pos, blockState));
	}

	/**
	 * fill one block if required and return true, if not required, return false
	 * 
	 * @param item the item
	 * @return true or false
	 */
	private Boolean fillOneBlock(Item item) {
		for (ConstructionBlock cBlock : list) {
			if (!cBlock.isSet()) {
				Block block = Block.getBlockFromItem(item);
				boolean found = compareBlocks(cBlock.getBlockState().getBlock(), block);
				if (found) {
					cBlock.setSet(true);
					// abort to speed up
					return true;
				}
			}
		}
		return false;
	}

	private Boolean compareBlocks(Block b1, Block b2) {
		ResourceLocation l1 = b1.getRegistryName();
		ResourceLocation l2 = b2.getRegistryName();
		boolean torchHack = l1.toString().contains("torch") && l2.toString().contains("torch"); // wall_torch if placed on
																																														// side, torch in inventory
		return l1.equals(l2) || torchHack;
	}

	/**
	 * fill the block requirements by this item stack's elements
	 * 
	 * @param itemStack the itemStack
	 */
	private void fulfillBlockRequirement(ItemStack itemStack) {
		Integer used = 0;
		for (Integer i = 0; i < itemStack.getCount(); i++) {
			used += fillOneBlock(itemStack.getItem()) ? 1 : 0;
		}
		itemStack.setCount(itemStack.getCount() - used);
	}

	/**
	 * get a list of blocks that are not filled in yet (left over building plan
	 * requirements)
	 * 
	 * @return a list of blocks (an empty one at least)
	 */
	public Map<Block, Integer> getAllMissingBlocks() {
		Map<Block, Integer> map = new HashMap<>();
		for (ConstructionBlock cBlock : list) {
			if (!cBlock.isSet()) {
				Integer amount = map.get(cBlock.getBlockState().getBlock());
				if (amount == null) {
					amount = 0;
				}
				amount++;
				map.put(cBlock.getBlockState().getBlock(), amount);
			}
		}
		return map;
	}

	public List<ConstructionBlock> getList() {
		return list;
	}

	/**
	 * fill blocks in the building plan from the user's inventory
	 * 
	 * @param player the player with an inventory
	 */
	public void fillBlocksFromInventory(PlayerEntity player) {
		PlayerInventory inventory = player.inventory;
		NonNullList<ItemStack> mainInventory = inventory.mainInventory;
		Iterator<ItemStack> i = mainInventory.iterator();
		while (i.hasNext()) {
			fulfillBlockRequirement(i.next());
		}
	}

	/**
	 * do only serialize what is needed here
	 * 
	 * @return the serialized byte array
	 */
	public byte[] serialize() {
		ConstructionBeanByteArray cbba = new ConstructionBeanByteArray();
		for (ConstructionBlock block : list) {
			cbba.add(block.toByteArray());
		}
		return cbba.getByteArray();
	}

	/**
	 * do only deserialize what is needed here
	 * 
	 * @param nbt the byte array
	 * @return
	 */
	public static final ConstructionBean deserialize(PlayerEntity player, byte[] nbt) {
		ConstructionBeanByteArray cbba = new ConstructionBeanByteArray(nbt);
		List<ConstructionBlock> list = new ArrayList<>();
		for (byte[] elem : cbba.getList()) {
			ConstructionBlock block = ConstructionBlock.fromByteArray(player, elem);
			if (block != null) {
				list.add(block);
			}
		}
		return new ConstructionBean(list);
	}

	/**
	 * create a short summary about this building plan
	 * 
	 * @return the summary
	 */
	public String toSummary() {
		StringBuilder buf = new StringBuilder();
		buf.append(name).append(", ");
		buf.append(list.size()).append(" blocks");
		return buf.toString();
	}

	/**
	 * rotate counterclockwise
	 * 
	 * @param player the player
	 * @param world  the world
	 */
	public void rotateCounterclockwise(PlayerEntity player, World world) {
		for (ConstructionBlock block : list) {
			block.resetBlockPos(block.getPos().getZ(), block.getPos().getY(), -block.getPos().getX());
			block.rotate(world, Rotation.COUNTERCLOCKWISE_90);
		}
		if (world.isRemote) {
			player.sendMessage(new TranslationTextComponent("msg.buildingplan.rotatecounterclockwise"));
		}
	}

	/**
	 * rotate clockwise
	 * 
	 * @param player the player
	 * @param world  the world
	 */
	public void rotateClockwise(PlayerEntity player, World world) {
		for (ConstructionBlock block : list) {
			block.resetBlockPos(-block.getPos().getZ(), block.getPos().getY(), block.getPos().getX());
			block.rotate(world, Rotation.CLOCKWISE_90);
		}
		if (world.isRemote) {
			player.sendMessage(new TranslationTextComponent("msg.buildingplan.rotateclockwise"));
		}
	}

	/**
	 * reverse x
	 */
	public void mirrorHorizontal(PlayerEntity player, World world) {
		for (ConstructionBlock block : list) {
			block.resetBlockPos(-block.getPos().getX(), block.getPos().getY(), block.getPos().getZ());
			block.mirror(Mirror.FRONT_BACK);
		}
		ITextComponent msg = new TranslationTextComponent("msg.buildingplan.mirrorhorizontal");
		if (world.isRemote) {
			player.sendMessage(msg);
		}
	}

	/**
	 * reverse z
	 */
	public void mirrorVertical(PlayerEntity player, World world) {
		for (ConstructionBlock block : list) {
			block.resetBlockPos(block.getPos().getX(), block.getPos().getY(), -block.getPos().getZ());
			block.mirror(Mirror.LEFT_RIGHT);
		}
		ITextComponent msg = new TranslationTextComponent("msg.buildingplan.mirrorvertical");
		if (world.isRemote) {
			player.sendMessage(msg);
		}
	}

	/**
	 * drop all blocks in building plan
	 * 
	 * @param world the world
	 * @param pos   the position to drop it to
	 */
	public void dropBlocksOntoGround(World world, BlockPos pos) {
		for (ConstructionBlock c : list) {
			if (c.isSet()) {
				TileEntity tileEntity = world.getTileEntity(pos);
				MinecraftServer server = world.getServer();
				Dimension dimension = world.getDimension();
				DimensionType type = dimension.getType();
				if (server != null) { // only on server side
					ServerWorld serverWorld = server.getWorld(type);
					List<ItemStack> drops = Block.getDrops(c.getBlockState(), serverWorld, pos, tileEntity);
					for (ItemStack stack : drops) {
						Block.spawnAsEntity(world, pos, stack);
					}
				}
				c.setSet(false); // also on client side
			}
		}
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * add i to each block's y axis
	 * 
	 * @param player the player
	 * @param world  the world
	 * @param y      the summand
	 */
	public void move(PlayerEntity player, World world, int y) {
		for (ConstructionBlock block : list) {
			block.resetBlockPos(block.getPos().getX(), block.getPos().getY() + y, block.getPos().getZ());
		}
		ITextComponent msg = new TranslationTextComponent("msg.buildingplan.move", y);
		if (world.isRemote) {
			player.sendMessage(msg);
		}
	}

	/**
	 * try to build construction in the world; if there is a reason not to do so,
	 * fail completely
	 * 
	 * @param worldIn  the world
	 * @param playerIn the player (for notifications)
	 * @param pos      the position of the block on that top the building will be
	 *                 created if possible
	 */
	public void buildInWorldOrFail(World worldIn, PlayerEntity playerIn, BlockPos pos) {
		Map<Block, Integer> map = getAllMissingBlocks();
		Block forbiddenBlock = findForbiddenBlock(worldIn, pos.up(), list, Blocks.BEDROCK, Blocks.OBSIDIAN);
		String playerInTheWay = findPlayerInTheWay(worldIn, pos.up(), list);
		if (forbiddenBlock != null) {
			if (worldIn.isRemote) {
				playerIn.sendMessage(
						new TranslationTextComponent("msg.buildingplan.failonblock", forbiddenBlock.getNameTextComponent()));
			}
		} else if (playerInTheWay != null) {
			if (worldIn.isRemote) {
				playerIn.sendMessage(new TranslationTextComponent("msg.buildingplan.failonplayer", playerInTheWay));
			}
		} else {
			if (map.size() < 1) {
				build(worldIn, pos.up());
			} else {
				if (worldIn.isRemote) {
					StringBuilder buf = new StringBuilder();
					for (Map.Entry<Block, Integer> entry : map.entrySet()) {
						buf.append(entry.getValue()).append("x ");
						ITextComponent name = entry.getKey().getNameTextComponent();
						buf.append(name.getFormattedText()).append(", ");
					}
					String mes = buf.toString();
					if (mes.length() > 1) { // remove the trailing ,
						mes = mes.substring(0, mes.length() - 2);
					}
					ITextComponent msg = new TranslationTextComponent("msg.buildingplan.missing", mes);
					playerIn.sendMessage(msg);
				}
			}
		}
	}

	/**
	 * check if player is in the way of the building area
	 * 
	 * @param worldIn the world
	 * @param pos     the block to build from
	 * @param list    the list of blocks to get its boundaries
	 * @return the first found player name or null if noone is in the way
	 */
	private String findPlayerInTheWay(World worldIn, BlockPos pos, List<ConstructionBlock> list) {
		List<? extends PlayerEntity> players = worldIn.getPlayers();
		Map<EnumExtreme, BlockPos> map = getBoundaries();
		int minX = map.get(EnumExtreme.MIN).getX() + pos.getX();
		int minY = map.get(EnumExtreme.MIN).getY() + pos.getY();
		int minZ = map.get(EnumExtreme.MIN).getZ() + pos.getZ();
		int maxX = map.get(EnumExtreme.MAX).getX() + pos.getX();
		int maxY = map.get(EnumExtreme.MAX).getY() + pos.getY();
		int maxZ = map.get(EnumExtreme.MAX).getZ() + pos.getZ();
		for (PlayerEntity player : players) {
			if (playerIsInWorldBox(worldIn, player, minX, minY, minZ, maxX, maxY, maxZ)) {
				return player.getName().getString();
			}
		}
		return null;
	}

	/**
	 * check if pos of player is in box of minx,miny,minz : maxx, maxy, maxz in the
	 * expected world
	 * 
	 * @param world
	 * @param player
	 * @param minX
	 * @param minY
	 * @param minZ
	 * @param maxX
	 * @param maxY
	 * @param maxZ
	 * @return
	 */
	private boolean playerIsInWorldBox(World world, PlayerEntity player, int minX, int minY, int minZ, int maxX, int maxY,
			int maxZ) {
		BlockPos pos = player.getPosition();
		boolean result = false;
		if (player.world.equals(world)) {
			result = checkBoundaries(pos.getX(), pos.getY(), pos.getZ(), minX, minY, minZ, maxX, maxY, maxZ);
		}
		return result;
	}

	protected boolean checkBoundaries(int x, int y, int z, int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
		boolean failX = minX <= x && maxX >= x;
		boolean failY = minY <= y && maxY >= y;
		boolean failZ = minZ <= z && maxZ >= z;
		return failX && failY && failZ;
	}

	/**
	 * check if a block replaced by the blocks is forbidden
	 * 
	 * @param worldIn         the world
	 * @param pos             the position to start from
	 * @param blocks          the list of planned blocks
	 * @param forbiddenBlocks the forbidden blocks
	 * @return the first forbidden block name if any, null otherwise
	 */
	private Block findForbiddenBlock(World worldIn, BlockPos pos, List<ConstructionBlock> blocks,
			Block... forbiddenBlocks) {
		for (ConstructionBlock block : blocks) {
			BlockPos worldPos = pos.add(block.getPos());
			for (Block forbidden : forbiddenBlocks) {
				Block current = worldIn.getBlockState(worldPos).getBlock();
				if (areBlocksEqual(forbidden, current)) {
					return forbidden;
				}
			}
		}
		return null;
	}

	/**
	 * compare two blocks and return true if they are equal
	 * 
	 * @param a block
	 * @param b block
	 * @return true or false
	 */
	private boolean areBlocksEqual(Block a, Block b) {
		if (a == null) {
			return b == null;
		} else if (b == null) {
			return false;
		}
		ResourceLocation rla = a.getRegistryName();
		ResourceLocation rlb = b.getRegistryName();
		return rla == null ? rlb == null : rla.compareTo(rlb) == 0;
	}

	/**
	 * get number of blocks in this plan
	 * 
	 * @return number of blocks; 0 should not occur but if a construction is built
	 *         out of construction border blocks
	 */
	public int getNumberOfTotalBlocks() {
		return list.size();
	}

	/**
	 * count the number of missing blocks
	 * 
	 * @return the number of missing blocks; if all are missing
	 */
	public int getNumberOfMissingBlocks() {
		Map<Block, Integer> map = getAllMissingBlocks();
		int sum = 0;
		for (Integer i : map.values()) {
			sum += i;
		}
		return sum;
	}

	/**
	 * check if all blocks for the building plan are available in it
	 * 
	 * @return false or true
	 */
	public boolean isFilled() {
		Map<Block, Integer> map = getAllMissingBlocks();
		return map.isEmpty();
	}

	/**
	 * TODO: implement
	 * 
	 * @param trashStack
	 * @param newStack
	 * @return
	 */
	public String replaceAllStacksOf(ItemStack trashStack, ItemStack newStack) {
		return "not yet implemented";
	}
}
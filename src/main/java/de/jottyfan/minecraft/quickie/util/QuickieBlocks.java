package de.jottyfan.minecraft.quickie.util;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.blocks.BlockConstructionBorder;
import de.jottyfan.minecraft.quickie.blocks.BlockDirtSalpeter;
import de.jottyfan.minecraft.quickie.blocks.BlockItemhoarder;
import de.jottyfan.minecraft.quickie.blocks.BlockLavahoarder;
import de.jottyfan.minecraft.quickie.blocks.BlockManiputateConstructionPlan;
import de.jottyfan.minecraft.quickie.blocks.BlockMenu;
import de.jottyfan.minecraft.quickie.blocks.BlockOreNetherSulphor;
import de.jottyfan.minecraft.quickie.blocks.BlockOreSalpeter;
import de.jottyfan.minecraft.quickie.blocks.BlockOreSandSalpeter;
import de.jottyfan.minecraft.quickie.blocks.BlockOreSulphor;
import de.jottyfan.minecraft.quickie.blocks.BlockSandSalpeter;
import net.minecraft.block.Block;
import net.minecraft.block.Block.Properties;
import net.minecraft.block.material.Material;
import net.minecraftforge.registries.ObjectHolder;

/**
 * 
 * @author jotty
 *
 */
@ObjectHolder(QuickieMod.MODID)
public class QuickieBlocks {

	public static final BlockOreSulphor ORE_SULPHOR = new BlockOreSulphor(Properties.create(Material.ROCK));
	public static final BlockOreNetherSulphor ORE_NETHER_SULPHOR = new BlockOreNetherSulphor(
			Properties.create(Material.ROCK));
	public static final BlockOreSalpeter ORE_SALPETER = new BlockOreSalpeter(Properties.create(Material.ROCK));
	public static final BlockOreSandSalpeter ORE_SAND_SALPETER = new BlockOreSandSalpeter(
			Properties.create(Material.ROCK));
	public static final BlockDirtSalpeter DIRT_SALPETER = new BlockDirtSalpeter(Properties.create(Material.ROCK));
	public static final BlockSandSalpeter SAND_SALPETER = new BlockSandSalpeter(Properties.create(Material.ROCK));

	public static final BlockConstructionBorder CONSTRUCTIONBORDER = new BlockConstructionBorder(
			Properties.create(Material.WOOD).hardnessAndResistance(0.1f));

	public static final BlockManiputateConstructionPlan ROTATECLOCKWISE = new BlockManiputateConstructionPlan(
			"rotateclockwise");
	public static final BlockManiputateConstructionPlan ROTATECOUNTERCLOCKWISE = new BlockManiputateConstructionPlan(
			"rotatecounterclockwise");
	public static final BlockManiputateConstructionPlan MIRRORHORIZONTAL = new BlockManiputateConstructionPlan(
			"mirrorhorizontal");
	public static final BlockManiputateConstructionPlan MIRRORVERTICAL = new BlockManiputateConstructionPlan(
			"mirrorvertical");
	public static final BlockManiputateConstructionPlan MOVEUP = new BlockManiputateConstructionPlan("moveup");
	public static final BlockManiputateConstructionPlan MOVEDOWN = new BlockManiputateConstructionPlan("movedown");
	public static final BlockMenu MENU = new BlockMenu(Properties.create(Material.ROCK));
	public static final BlockLavahoarder LAVAHOARDER = new BlockLavahoarder(Properties.create(Material.ROCK));
	public static final BlockItemhoarder ITEMHOARDER = new BlockItemhoarder(Properties.create(Material.ROCK));

	public static final Block[] BLOCKS = { ORE_SULPHOR, ORE_NETHER_SULPHOR, ORE_SALPETER, ORE_SAND_SALPETER,
			DIRT_SALPETER, SAND_SALPETER, CONSTRUCTIONBORDER, ROTATECLOCKWISE, ROTATECOUNTERCLOCKWISE, MIRRORHORIZONTAL,
			MIRRORVERTICAL, MOVEUP, MOVEDOWN, MENU, LAVAHOARDER, ITEMHOARDER };
}

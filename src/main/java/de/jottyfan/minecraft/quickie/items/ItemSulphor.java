package de.jottyfan.minecraft.quickie.items;

import de.jottyfan.minecraft.quickie.QuickieMod;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

/**
 * 
 * @author jotty
 *
 */
public class ItemSulphor extends Item {

	public ItemSulphor() {
		super((new Item.Properties()).group(ItemGroup.REDSTONE));
		super.setRegistryName(QuickieMod.MODID, "sulphor");
	}
}

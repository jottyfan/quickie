package de.jottyfan.minecraft.quickie.items;

import java.util.List;

import com.google.common.collect.Lists;

import de.jottyfan.minecraft.quickie.QuickieMod;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTier;
import net.minecraft.item.ShovelItem;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class ToolSpeedpowderShovel extends ShovelItem implements ToolRangeable {

	public HarvestRange range;

	public ToolSpeedpowderShovel() {
		super(ItemTier.DIAMOND, 4, 2.0f, (new Item.Properties()).group(ItemGroup.TOOLS));
		this.range = new HarvestRange(2);
		super.setRegistryName(QuickieMod.MODID, "speedpowdershovel");
	}

	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state) {
		return BIOMESOPLENTY_SHOVEL.contains(state.getBlock().getRegistryName()) ? this.efficiency
				: super.getDestroySpeed(stack, state);
	}

	@Override
	public HarvestRange getRange() {
		return range;
	}

	@Override
	public boolean canBreakNeigbbors(BlockState blockIn) {
		return SHOVEL_EFFECTIVE_ON.contains(blockIn.getBlock())
				|| BIOMESOPLENTY_SHOVEL.contains(blockIn.getBlock().getRegistryName());
	}

	@Override
	public List<Block> getBlockList(Block block) {
		return Lists.newArrayList(block);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		CommonToolCode.onItemRightClick(worldIn, playerIn, handIn);
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		CommonToolCode.addInformation(stack, worldIn, tooltip, flagIn);
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}
}

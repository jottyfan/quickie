package de.jottyfan.minecraft.quickie.tileentities;

import java.util.Set;
import java.util.function.Supplier;

import com.mojang.datafixers.types.Type;

import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntityType;

/**
 * 
 * @author jotty
 *
 */
public class ItemhoarderTileentityType extends TileEntityType<ItemhoarderTileentity> {

	public ItemhoarderTileentityType(Supplier<? extends ItemhoarderTileentity> factory, Set<Block> validBlocks,
			Type<?> datafixerType) {
		super(factory, validBlocks, datafixerType);
	}

}
